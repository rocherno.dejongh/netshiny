library(colourpicker)
library(dplyr) 
library(DT)
library(future) 
library(future.callr)
library(ggplot2) 
library(graphics) 
library(igraph) 
library(ipc) 
library(Matrix) 
library(methods) 
library(netgwas) 
library(plotly) 
library(promises)
library(scales)
library(shinyBS)
library(shiny)
library(shinydashboard)
library(shinycssloaders)
library(shinyjs)
library(shinyscreenshot)
library(shinyWidgets)
library(stats)
library(tools)
library(utils)
library(visNetwork) 
library(withr) 

source("utils_data_processing.R", local = TRUE)
source("utils_global.R", local = TRUE)
source("utils_plots.R", local = TRUE)

future::plan(future.callr::callr)

load("objects.RData")

###HARDCODED PART###
Net.obj <- envs
mapping <- map_mks
resamples <- boots
####################

sett_nms <- NULL
all_node_nms <- NULL
to_reconstruct <- FALSE
df_settings <- NULL

if(!is.null(Net.obj)){
  if(is.null(names(Net.obj))){
    message("Names not provided, default names will be used")
    sett_nms <- sprintf("Environment %s", 1:length(Net.obj))
    names(Net.obj) <- sett_nms
  }
  else{
    sett_nms <- names(Net.obj)
  }
  for(i in 1:length(Net.obj)){
    if(is.null(dimnames(Net.obj[[i]])[[1]])){
      if(is.null(dimnames(Net.obj[[i]])[[2]])){
        dimnames(Net.obj[[i]]) <- list(paste("Node", 1:nrow(Net.obj[[i]])), paste("Node", 1:nrow(Net.obj[[i]])))
      }
      else{
        dimnames(Net.obj[[i]])[[1]] <- dimnames(Net.obj[[i]])[[2]]
      }
    }
    else if(is.null(dimnames(Net.obj[[i]])[[2]])){
      dimnames(Net.obj[[i]])[[2]] <- dimnames(Net.obj[[i]])[[1]]
    }
  }
  n_nodes <- max(unlist(sapply(Net.obj, function(x) dim(x)[[1]])))
  all_node_nms <- unique(unlist(lapply(Net.obj, function(x) dimnames(x)[[2]])))
  for(i in 1:length(Net.obj)){
    curr_nm <- sett_nms[i]
    if(dim(Net.obj[[curr_nm]])[[1]] == dim(Net.obj[[curr_nm]])[[2]]){
      Net.obj[[curr_nm]] <- as.matrix(Net.obj[[curr_nm]])
      Net.obj[[curr_nm]] <- Matrix::Matrix(Net.obj[[curr_nm]], sparse = TRUE)
    }
    else{
      to_reconstruct <- TRUE
      df_settings[[curr_nm]] <- data.frame(Net.obj[[curr_nm]])
      Net.obj[[curr_nm]] <- NULL
    }
  }
}

sidebar <- shinydashboard::dashboardSidebar(
  shinyWidgets::setSliderColor(rep("#007c00", 3), c(1, 2, 3)),
  shinyjs::useShinyjs(),
  shinydashboard::sidebarMenu(id = "tabs",
                              shinydashboard::menuItem("Networks", tabName = "networks", icon = shiny::icon("project-diagram")),
                              shinydashboard::menuItem("Summary Statistics", tabName = "summ_stats", icon = shiny::icon("stats", lib = "glyphicon")),
                              shinydashboard::menuItem("Weights Analysis", tabName = "net", icon = shiny::icon("chart-line")),
                              shinydashboard::menuItem("Centrality Measures", tabName = "centrality", icon = shiny::icon("align-center")),
                              shinydashboard::menuItem("Net Diffs", tabName = "differences", icon = shiny::icon("compress")),
                              shinydashboard::menuItem("Community Detection", tabName = "comm_detect", icon = shiny::icon("chart-bar")),
                              shinydashboard::menuItem("Uncertainty Check", tabName = "unc_check", icon = shiny::icon("chart-area")),
                              shiny::sliderInput(inputId = "cor_t", "Partial Correlations Traits", min = 0, max = 1, value = 0),
                              shiny::sliderInput(inputId = "cor_m", "Partial Correlations Markers", min = 0, max = 1, value = 0),
                              shiny::selectInput("net1", "Left Panel", sett_nms, selected = sett_nms[1]),
                              shiny::selectInput("net2", "Right Panel", sett_nms, selected = sett_nms[2]),
                              shiny::selectInput("layout", "Layout", c("Automatic",
                                                                       "Circle",
                                                                       "Fruchterman-Reingold",
                                                                       "Grid.2D",
                                                                       "Kamada-Kawai" = "kk",
                                                                       "Tree"), selected = "Automatic"),
                              shiny::sliderInput("roundness", "Edge Curviness", min = 0, max = 1, value = 0),
                              shiny::selectInput("sets_selin", "Operation", c("Union", "Intersection", "Complement"), selected = "Union"),
                              shiny::selectInput("marker", "Markers", all_node_nms),
                              shinyWidgets::searchInput(inputId = "meas_butt", label = "Add Statistic", placeholder = "func, arg1; func2", btnSearch = shiny::icon("search"), btnReset = shiny::icon("times"), width = "100%"),
                              shinyBS::bsTooltip(id = "meas_butt", title = "Add arguments for function by separting by commas, add addtional function by separating by ;", options = list(hover = "auto")),
                              shiny::selectInput("cluster_algs", "Clustering Algorithm", c("Fast Greedy", "Edge Betweenness"), selected = "Fast Greedy"),
                              shiny::tags$head(
                                shiny::tags$style(shiny::HTML('#subgraph{color:black; background-color:#F5F2F2}
                                  #customize{color:black; background-color:#F5F2F2}
                                  #print_network{color:black; background-color:#F5F2F2}
                                  #refresh{color:black; background-color:#F5F2F2}'))
                              ),
                              shiny::splitLayout(cellWidths = c("45%", "22%", "22%"),
                                                 shinyWidgets::actionBttn("subgraph", label = "subgraph", style = "simple", size = "sm"),
                                                 shinyWidgets::actionBttn("print_network", label = NULL, icon = shiny::icon("print"), style = "simple", size = "sm"),
                                                 shinyWidgets::actionBttn("refresh", label = NULL, icon = shiny::icon("redo"), style = "simple", size = "sm")
                              ),
                              shinyWidgets::actionBttn("customize", label = "customize", style = "simple", size = "sm")
                              
  )
)

withr::local_options(list(spinner.color = "#007c00", spinner.size = 2))
shinyWidgets::useSweetAlert()

body <- shinydashboard::dashboardBody(
  shiny::tags$head(shiny::tags$style(".modal{ overflow-y:auto}")),
  shiny::tags$head(shiny::tags$style(type = "text/css", ".modal-backdrop { z-index: -1}")),
  custombsModal("modalStartup_step1", "Files for Network Reconstruction", "nothin", size = "large",
                shiny::fileInput("files_upload", "Choose Files",
                                 multiple = TRUE,
                                 accept = c(
                                   'text/csv',
                                   'text/comma-separated-values,text/plain',
                                   '.csv',
                                   '.xls',
                                   '.xlsx'
                                 )),
                shiny::tags$hr(),
                shiny::fluidRow(
                  shiny::column(width = 3, shiny::checkboxInput("header", "Header", TRUE)),
                  shiny::column(width = 3, shiny::radioButtons("sep", "Separator",
                                                               choices = c(Comma = ",",
                                                                           Semicolon = ";",
                                                                           Tab = "\t"),
                                                               selected = ",")),
                  shiny::column(width = 3, shiny::radioButtons("quote", "Quote",
                                                               choices = c(None = "",
                                                                           "Double Quote" = '"',
                                                                           "Single Quote" = "'"),
                                                               selected = '"')),
                  shiny::column(width = 3, shiny::textInput("exc_columns", "Columns To Exclude (separate by space)",
                                                            value = ""))
                ),
                shiny::tags$hr(),
                shiny::selectInput("currFile", "File", choices = NULL),
                DT::dataTableOutput("tbl"),
                shiny::fluidRow(
                  shiny::column(width = 12, offset = 10,
                                shiny::actionButton(inputId = "nextButton_startup", label = "Next")
                  )
                ),
                shiny::tags$head(shiny::tags$style("#modalStartup_step1 .modal-footer{ display:none}"))),
  custombsModal("modalStartup_reconstruction", "Arguments for Network Reconstruction", "nextButton_startup", size = "large",
                shiny::selectInput("file_names", "Data Files", choices = NULL),
                shinyWidgets::awesomeCheckbox("adv_op", "Show advanced option", value = FALSE),
                shiny::tags$hr(),
                shiny::fluidRow(
                  shiny::column(width = 6,
                                shiny::tags$h3("Arguments for Network Reconstruction", id = "net_recon_tag"),
                                shinyWidgets::pickerInput(inputId = "net_method_start", label = "method", choices = c("Gibbs sampling" = "gibbs",
                                                                                                                      "approximation method" = "approx",
                                                                                                                      "nonparanormal" = "npn"), selected = "npn"),
                                shiny::textInput(inputId = "net_rho_start", label = "rho (decreasing sequence of non-negative numbers that control the sparsity level)", value = "NULL"),
                                shiny::numericInput(inputId = "net_n.rho_start", label = "n.rho (number of regularization parameters)", value = 10),
                                shiny::numericInput(inputId = "net_rho.ratio_start", label = "rho.ratio (distance between the elements of rho sequence)",
                                                    value = 0.3, min = 0, max = 1, step = 0.1),
                                shiny::textInput(inputId = "net_ncores_start", label = "ncores (number of cores to use for the calculations)", value = "1"),
                                shiny::numericInput(inputId = "net_em.iter_start", label = "em.iter (number of EM iterations)", value = 5),
                                shiny::numericInput(inputId = "net_em.tol_start", label = "em.tol (criteria to stop the EM iterations)",
                                                    value = 0.001, min = 0.001, max = 1, step = 0.001)
                  ),
                  shiny::column(width = 6,
                                shiny::tags$h3("Arguments for Selecting Optimal Network", id = "sel_net_tag"),
                                shiny::textInput(inputId = "sel_opt.index_start", label = "opt.index (manually choose an optimal graph from the graph path)", value = "NULL"),
                                shinyWidgets::pickerInput(inputId = "sel_criteria_start", label = "criteria (model selection criteria)", choices = c("NULL", "ebic", "aic"), selected = "NULL"),
                                shiny::numericInput(inputId = "sel_ebic.gamma_start", label = "ebic.gamma (tuning parameter for ebic)",
                                                    value = 0.5, min = 0, max = 1, step = 0.1),
                                shiny::textInput(inputId = "sel_ncores_start", label = "ncores (number of cores to use for the calculations)", value = "1")
                                
                  )
                ),
                shiny::tags$hr(),
                shiny::fluidRow(
                  shiny::column(width = 2,
                                shiny::actionButton(inputId = "prevButton_reconstruction", label = "Previous")
                  ),
                  shiny::column(width = 2, offset = 8,
                                shiny::actionButton(inputId = "startup_run", label = "Run", style = "color: #fff; background-color: #007c00")
                  )
                ),
                shiny::tags$head(shiny::tags$style("#modalStartup_reconstruction .modal-footer{ display:none}"))),
  ##nothing2 needs to be removed
  custombsModal("startup_mapping", "File for Mapping of Nodes", "nothing2", size = "large",
                shiny::fileInput("mapping_upload", "Choose File for Mapping",
                                 multiple = FALSE,
                                 accept = c(
                                   'text/csv',
                                   'text/comma-separated-values,text/plain',
                                   '.csv',
                                   '.xls',
                                   '.xlsx'
                                 )),
                shiny::tags$hr(),
                shiny::fluidRow(
                  shiny::column(width = 3, shiny::checkboxInput("header_mapping", "Header", TRUE)
                  ),
                  shiny::column(width = 3, shiny::radioButtons("sep_mapping", "Separator",
                                                               choices = c(Comma = ",",
                                                                           Semicolon = ";",
                                                                           Tab = "\t"),
                                                               selected = ",")),
                  shiny::column(width = 3, shiny::radioButtons("quote_mapping", "Quote",
                                                               choices = c(None = "",
                                                                           "Double Quote" = '"',
                                                                           "Single Quote" = "'"),
                                                               selected = '"')),
                  shiny::column(width = 3, shiny::textInput("exc_columns_mapping", "Columns To Exclude (separate by space)",
                                                            value = ""))
                ),
                shiny::tags$hr(),
                DT::dataTableOutput("tbl_mapping"),
                shiny::fluidRow(
                  shiny::column(width = 2,
                                shiny::actionButton(inputId = "prevButton_mapping", label = "Previous")
                  ),
                  shiny::column(width = 2, offset = 8,
                                shiny::actionButton(inputId = "nextButton_mapping", label = "Next")
                  )
                ),
                shiny::tags$head(shiny::tags$style("#startup_mapping .modal-footer{ display:none}"))),
  custombsModal("data_settings", "Options for Mapping of Nodes", "nextButton_mapping", size = "large",
                shinyWidgets::switchInput("gxe_mode", label = "GxE Mode", labelWidth = "80px", onLabel = "YES", offLabel = "NO", value = TRUE),
                shiny::textInput("net_names", label = "Environment Names:", value = NULL),
                shiny::numericInput(inputId = "n_traits", label = "Number of Traits:", value = NULL, min = 1),
                shiny::textInput("trait_types", value = NULL, label = "Grouping of Traits:", placeholder =  "e.g. Harvest:4, Intermediate:5, Physiological:10"),
                shiny::fluidRow(
                  shiny::column(width = 2,
                                shiny::actionButton(inputId = "prevButton_data_settings", label = "Previous")
                  ),
                  shiny::column(width = 2, offset = 8,
                                shiny::actionButton(inputId = "finish_data_settings", label = "Done")
                  )
                ),
                shiny::tags$head(shiny::tags$style("#data_settings .modal-footer{ display:none}"))
  ),
  shiny::tags$head(shiny::tags$style(HTML('
              /* logo */
        .skin-blue .main-header .logo {
          background-color: #306ea1;
        }

        /* logo when hovered */
        .skin-blue .main-header .logo:hover {
          background-color: #306ea1;
        }

        /* navbar (rest of the header) */
        .skin-blue .main-header .navbar {
          background-color: #306ea1;
        }

        /* main sidebar */
        .skin-blue .main-sidebar {
          background-color: #306ea1;
        }

        /* active selected tab in the sidebarmenu */
        .skin-blue .main-sidebar .sidebar .sidebar-menu .active a{
          background-color: #F5F2F2;
        }

        /* other links in the sidebarmenu */
        .skin-blue .main-sidebar .sidebar .sidebar-menu a{
          background-color: #007c00;
          color: #000000;
        }

        /* other links in the sidebarmenu when hovered */
        .skin-blue .main-sidebar .sidebar .sidebar-menu a:hover{
          background-color: #9acd32;
        }

        /* toggle button when hovered  */
        .skin-blue .main-header .navbar .sidebar-toggle:hover{
          background-color: #007c00;
        }

        /* body */
        .content-wrapper, .right-side {
          background-color: #F5F2F2;
        }

        .multicol {
           -webkit-column-count: 3; /* Chrome, Safari, Opera */
           -moz-column-count: 3;    /* Firefox */
           column-count: 3;
           -moz-column-fill: auto;
           -column-fill: auto;
        }

       .shiny-split-layout > div {
          overflow: visible;
       }

    '))),
  shinydashboard::tabItems(
    
    #Networks tab
    shinydashboard::tabItem(tabName = "networks",
                            shiny::splitLayout(cellWidths = c("50%", "50%"),
                                               style = "padding: 0px",
                                               cellArgs = list(style = "border-right: 1px solid silver"),
                                               shinycssloaders::withSpinner(visNetwork::visNetworkOutput("network_proxy_nodes", height = "1000px")),
                                               shinycssloaders::withSpinner(visNetwork::visNetworkOutput("network_proxy_nodes_2", height = "1000px"))),
    ),
    
    #Summary Statistics tab
    shinydashboard::tabItem(tabName = "summ_stats",
                            shiny::tabsetPanel(id = "tabs_summ_stats",
                                               shiny::tabPanel("Metrics", shinycssloaders::withSpinner(shiny::plotOutput("summary_statistics", height = "750px"))),
                                               shiny::tabPanel(title = shiny::textOutput("title_par_cors"),
                                                               shinyWidgets::dropdownButton(
                                                                 shiny::tags$h3("Plot Settings"),
                                                                 shiny::numericInput(inputId = "par_cor_bins", label = "Bins", min = 0, value = 15),
                                                                 shiny::numericInput(inputId = "par_cor_breaks", label = "X-axis Breaks", min = 0, value = 20),
                                                                 circle = TRUE, status = "primary", icon = shiny::icon("cog"),
                                                                 tooltip = shinyWidgets::tooltipOptions(title = "Click to change plot's settings")
                                                               ),
                                                               shinycssloaders::withSpinner(shiny::plotOutput("par_cors", height = "750px")))
                            )
                            
    ),
    
    #Community Detection tab
    shinydashboard::tabItem(tabName = "comm_detect",
                            shiny::tabsetPanel(id = "tabs_comm_detect",
                                               shiny::tabPanel("Communities", shiny::splitLayout(cellWidths = c("50%", "50%"),
                                                                                                 style = "padding: 0px",
                                                                                                 cellArgs = list(style = "border-right: 1px solid silver"),
                                                                                                 shinycssloaders::withSpinner(shiny::plotOutput("comms_plot", height = "1000px")),
                                                                                                 shinycssloaders::withSpinner(shiny::plotOutput("comms_plot2", height = "1000px")))
                                               ),
                                               shiny::tabPanel("Modularity",  shinycssloaders::withSpinner(shiny::plotOutput("mods")))
                                               
                            )
    ),
    
    #Tab for Uncertainty check
    shinydashboard::tabItem(tabName = "unc_check",
                            shinyWidgets::dropdown(
                              shiny::fluidRow(
                                shiny::column(width = 6,
                                              shiny::tags$h3("Arguments for netphenogeno"),
                                              shinyWidgets::pickerInput(inputId = "net_method", label = "method", choices = c("gibbs", "approx",  "npn"), selected = "npn"),
                                              shiny::textInput(inputId = "net_rho", label = "rho (decreasing sequence of non-negative numbers that control the sparsity level)", value = "NULL"),
                                              shiny::numericInput(inputId = "net_n.rho", label = "n.rho (number of regularization parameters)", value = 10),
                                              shiny::numericInput(inputId = "net_rho.ratio", label = "rho.ratio (distance between the elements of rho sequence)", value = 0.3),
                                              shiny::textInput(inputId = "net_ncores", label = "ncores (number of cores to use for the calculations)", value = "1"),
                                              shiny::numericInput(inputId = "net_em.iter", label = "em.iter (number of EM iterations)", value = 5),
                                              shiny::numericInput(inputId = "net_em.tol", label = "em.tol (criteria to stop the EM iterations)", value = 0.001),
                                              shinyWidgets::actionBttn(inputId = "unc_check_run", label = "Run", style = "jelly", color = "danger")
                                ),
                                shiny::column(width = 6,
                                              shiny::tags$h3("Arguments for selectnet"),
                                              shiny::textInput(inputId = "sel_opt.index", label = "opt.index (manually choose an optimal graph from the graph path)", value = "NULL"),
                                              shinyWidgets::pickerInput(inputId = "sel_criteria", label = "criteria (model selection criteria)", choices = c("NULL", "ebic", "aic"), selected = "NULL"),
                                              shiny::numericInput(inputId = "sel_ebic.gamma", label = "ebic.gamma (tuning parameter for ebic)", value = 0.5),
                                              shiny::textInput(inputId = "sel_ncores", label = "ncores (number of cores to use for the calculations)", value = "1"),
                                              shiny::tags$h4("Bootsrap Method Parameters"),
                                              shiny::numericInput(inputId = "unc_check_n", label = "Number of resamples", value = 10)
                                              
                                )
                              ),
                              circle = TRUE, status = "primary", icon = shiny::icon("cog"), inputId = "dropdown_res",
                              tooltip = shinyWidgets::tooltipOptions(title = "Click to plot's settings")
                            ),
                            shinycssloaders::withSpinner(
                              plotly::plotlyOutput("unc_check_plot", height = "750px", width = "100%"),
                            )
    ),
    
    #Netphenogeno tab
    shinydashboard::tabItem(tabName = "net",
                            shinycssloaders::withSpinner(
                              plotly::plotlyOutput("net_plot", width = "100%", height = "750px")
                            )
                            
    ),
    
    #Centrality Measures Plots
    shinydashboard::tabItem(tabName = "centrality",
                            shinyWidgets::dropdownButton(
                              shiny::tags$h3("Plot Settings"),
                              shinyWidgets::materialSwitch(inputId = "cen_meas_col_switch", label = "Color by Group", value = TRUE, status = "primary"),
                              shiny::selectInput(inputId = "cen_meas_leg_pos", label = "Legend Position", choices = c("right", "left", "bottom", "top"), selected = "bottom"),
                              circle = TRUE, status = "primary", icon = shiny::icon("cog"),
                              tooltip = shinyWidgets::tooltipOptions(title = "Click to plot's settings")
                            ),
                            shinycssloaders::withSpinner(
                              shiny::plotOutput("centrality_plots", height = "750px")
                            )
    ),
    
    #Differences Tab
    shinydashboard::tabItem(tabName = "differences",
                            shiny::tabsetPanel(id = "tab_differences",
                                               shiny::tabPanel("Difference Networks",
                                                               shinycssloaders::withSpinner(visNetwork::visNetworkOutput("diff_nets", height = "750px"))),
                                               shiny::tabPanel("Network Distances",
                                                               shiny::fluidRow(
                                                                 shinydashboard::box(title = shiny::textOutput("title_dist_table"), width = 12, solidHeader = TRUE, status = "primary",
                                                                                     shinyWidgets::dropdownButton(
                                                                                       shiny::tags$h3("Table Settings"),
                                                                                       shinyWidgets::radioGroupButtons(inputId = "mat_type_table", label = "", choices = list("Adjacency", "Weighted"), selected = "Adjacency", justified = TRUE),
                                                                                       shiny::selectInput(inputId = "dist_meas_table", label = "Distance Measure", choices = c("Euclidean", "Manhattan", "Canberra", "Jaccard"), selected = "Euclidean"),
                                                                                       circle = TRUE, status = "primary", icon = shiny::icon("cog"), size = "sm", width = "300px",
                                                                                       tooltip = shinyWidgets::tooltipOptions(title = "Click to plot's settings")
                                                                                     ),
                                                                                     shinycssloaders::withSpinner(shiny::uiOutput("distances_table"))
                                                                 )
                                                               ),
                                                               shiny::fluidRow(
                                                                 shinydashboard::box(title = textOutput("title_dist_plot"), width = 12, solidHeader = TRUE, status = "primary",
                                                                                     shinyWidgets::dropdownButton(
                                                                                       shiny::tags$h3("Plot Settings"),
                                                                                       shinyWidgets::radioGroupButtons(inputId = "mat_type_plot", label = "", choices = c("Adjacency", "Weighted"), selected = "Adjacency", justified = TRUE),
                                                                                       shiny::selectInput(inputId = "dist_meas_plot", label = "Distance Measure", choices = c("Euclidean", "Manhattan", "Canberra", "Jaccard"), selected = "Euclidean", multiple = TRUE),
                                                                                       shiny::checkboxInput(inputId = "log_check", label = "Log Scale", value = FALSE),
                                                                                       circle = TRUE, status = "primary", icon = shiny::icon("cog"), size = "sm", width = "300px",
                                                                                       tooltip = shinyWidgets::tooltipOptions(title = "Click to plot's settings")
                                                                                     ),
                                                                                     shinycssloaders::withSpinner(shiny::plotOutput("distances_plot"))
                                                                 )
                                                               )
                                               ),
                                               shiny::tabPanel("Nodes Sets",
                                                               shinycssloaders::withSpinner(DT::dataTableOutput("diff_sets")))
                            )
    )
  )
)

# Put them together into a dashboardPage
ui <- shinydashboard::dashboardPage(
  shinydashboard::dashboardHeader(title = "netShiny",
                                  shiny::tags$li(shiny::actionLink("settings_button", label = "", icon = shiny::icon("cog")),
                                                 class = "dropdown"), titleWidth = 100),
  sidebar,
  body
)